source(findFile("scripts", "Libraries/Applicationflow.js"))
source(findFile("scripts", "Libraries/Global.js"))
source(findFile("scripts", "Libraries/Constants.js"))
source(findFile("scripts", "Libraries/Perf.js"))

function main()
{
    
    var objApp = new Application();
    var Global = new GlobalVariables();
    
    testSettings.logScreenshotOnPass = true;
    testSettings.logScreenshotOnError = true;
    testSettings.logScreenshotOnFail = true;
    startBrowser("http://www.google.de");
    waitForObject(":Google_BrowserTab");
    loadUrl(Global.Consumer);
    
    var bStatus = false;

    // automateLogin("xtuple", "nordicnaturals");
    test.pass("Start " + Global.Professional + " application.","Application has been successfully");    
    snooze(2);
  
    var dsCustomerDetails = testData.dataset("LoginDetails.tsv");
    test.log("data fetched");
    objApp.LoginCustomer(testData.field(dsCustomerDetails[objApp.getCustomerDetailsColNo("LoginUserName")],"ConsumerCustomer"),testData.field(dsCustomerDetails[objApp.getCustomerDetailsColNo("LoginPassword")],"ConsumerCustomer"));
    test.pass("Search for the customer " + testData.field(dsCustomerDetails[objApp.getCustomerDetailsColNo("LoginUserName")],"ConsumerCustomer"),"Customer details are being fetched successfully");
 var dsOrderDetails = testData.dataset("OrderDetails.tsv");
    
    objApp.selectingproduct(testData.field(dsOrderDetails[objApp.getOrderDetailsColNo("ProductQuantity")],"tst_TC04_ConsumerCustomerDiscount_0percent-1"), true, true);
    test.pass("Add new line item","New sales order has been added at the line level successfully");
    
    loadUrl(Global.carddetails);
    var dscarddetails=testData.dataset("carddetails.tsv");
    objApp.amountCarddetails(testData.field(dscarddetails[objApp.getcarddetails("Card_Type")],"details"),testData.field(dscarddetails[objApp.getcarddetails("Nameoncard")],"details"),testData.field(dscarddetails[objApp.getcarddetails("CardNumber")],"details"),testData.field(dscarddetails[objApp.getcarddetails("Expiration_month")],"details"),testData.field(dscarddetails[objApp.getcarddetails("Expiration_year")],"details"),testData.field(dscarddetails[objApp.getcarddetails("Security_Code")],"details"));
       
   /*bStatus=objApp.logotAccount();
  if(bStatus) test.pass("Logout from the application","Logout from application successfully");
  else test.fail("Logout from application","Unable to logout");*/
 }
